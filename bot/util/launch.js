
/**
 * @description module to start bot in production, using webhook
 * @requires VERCEL_URL a system environment variable provided by Vercel
 * @see https://vercel.com/docs/concepts/projects/environment-variables#system-environment-variables
 */
const production = async (bot) => {
	try {
		await bot.api.setWebhook(`${process.env.VERCEL_URL}/api/index`);
		console.log(`[SERVER] Bot starting webhook`);
	} catch (e) {
		console.error(e);
	}
};

const development = async (bot) => {
	try {
		await bot.api.deleteWebhook();
		console.log("[SERVER] Bot starting polling");
		await bot.start();
	} catch (e) {
		console.error(e);
	}
};

module.exports = {
	production,
	development,
};
