const processQuery = require('./processQuery');
const launch = require('./launch');

module.exports = {
	processQuery,
	launch,
};
