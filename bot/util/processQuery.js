const bent = require('bent');
/**
 * @todo use different urls for development and production
 */
//const getJSON = bent('GET', 'json', 'https://ergast.com/api/f1', 200);// ergast
const getJSON = bent('GET', 'json', 'https://f1alerts-1-p3324001.deta.app/f1', 200);// deta cache

/**
 * @see https://medium.com/@techno021/a-fix-to-the-unhandled-promise-rejection-warning-4fc0598896fe
 */
const processQuery = async (endpoint) => {
	try {
		const response = await getJSON( endpoint );
		//console.log(`response ${endpoint}`, response );
		return response;
	} catch (error) {// Handle rejection here
		console.error('processQuery :(', error)
	}
}

module.exports = processQuery;
