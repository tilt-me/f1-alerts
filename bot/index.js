'use strict';

//const { Bot } = require("grammy");
const bot = require("./core");
const botCommand = require("./command");
const botUtil = require("./util");
//const bot = new Bot(process.env.BOT_TOKEN);

/* 
bot.command("start", (ctx) => ctx.reply("Welcome! <b>Up and running</b>.", {
	parse_mode: "HTML",
}));
 */
bot.command('start', botCommand.start());
bot.command('nextround', botCommand.nextround());
bot.command('lastround', botCommand.lastround());
bot.command('driverstandings', botCommand.driverstandings());
bot.command('constructorstandings', botCommand.constructorstandings());
bot.command('racecalendar', botCommand.racecalendar());
bot.command('about', botCommand.about());

//bot.start();
console.log(process.env.NODE_ENV);
if (process.env.NODE_ENV === 'development') {
	botUtil.launch.development(bot);
} else {
	botUtil.launch.production(bot);
}
