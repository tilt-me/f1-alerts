# F1 Alerts #


## Developing ##

Start local server (and read the logs):
```
$ BOT_TOKEN='insert-bot-token-here' NODE_ENV=development node index
```


## Publishing ##

Publishing is handled by Vercel: every push to master will trigger a deployment.  

You need to set these variables on gitlab (under `Project > Settings > CI/CD > Variables`):

- VERCEL_ORG_ID: The ID of the user or team your Vercel project is owned by ("orgId"); if you have no group but just a user, org id is the same as user id, and you can get it from here:  
https://vercel.com/account

- VERCEL_PROJECT_ID: The ID of the Vercel project that you linked ("projectId"); you can get it from the Vercel Project Settings page:  
https://vercel.com/tilt/f1_alerts_bot/settings

- VERCEL_SCOPE: if you are a single user, it is the same as your user name; otherwise, could be one of the groups you belong to

- VERCEL_TOKEN: tokens allow other apps to control your whole account, you can generate one here:  
https://vercel.com/account/tokens


## Configuring bot ##

To configure this bot you need to chat with the BotFather on Telegram.
Here you should:
- set bot name, description etc.;
- list quick commands (/nextround, /lastround etc.).

(tip: you can use telegram desktop for faster bot editing.)

Currently available commands (copy and paste this list to BotFather):  
nextround - Next round schedule  
lastround - Last round results  
driverstandings - Driver standings  
constructorstandings - Constructor standings  
racecalendar - Race calendar  

Not listed:  
/about


## References ##

- https://github.com/neumanf/grammy-vercel-boilerplate
- https://github.com/WingLim/vercel-telegram-bot
- https://github.com/neumanf/grammy-vercel-boilerplate/blob/main/src/utils/launch.ts
- https://www.marclittlemore.com/serverless-telegram-chatbot-vercel/
- https://vercel.com/guides/getting-started-with-vercel-for-gitlab automatic deploy with gitlab + vercel
- https://vercel.com/docs/concepts/git/monorepos
- https://github.com/vercel/vercel/discussions/4853 Deploy to vercel via gitlab CI
