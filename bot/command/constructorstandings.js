const botUtil = require("../util");

const constructorstandings = () => async (ctx) => {
	try {
		
		const response = await botUtil.processQuery('/current/constructorStandings.json');
		
		let message = ``;
		message += `🏆 <b>Constructor standings</b> (round #${response.MRData.StandingsTable.StandingsLists[0].round} of ${response.MRData.StandingsTable.season} season):`;
		message += `\n`;
		
		response.MRData.StandingsTable.StandingsLists[0].ConstructorStandings.map( (standing, index) => {
			message += `\n#${standing.position} <b>${standing.Constructor.name}</b> <code>${standing.points}pts</code>`;
		});
		
		const option = {
			parse_mode: "HTML",
		};
		
		return ctx.reply(message, option);
		
	} catch(error) {
		console.error('constructorstandings :(', error)
	}
};

module.exports = constructorstandings;
