const botUtil = require("../util");

const racecalendar = () => async (ctx) => {
	try {
		
		const response = await botUtil.processQuery('/current.json');
		
		let message = ``;
		message += `<b>Race calendar</b> for ${response.MRData.RaceTable.season} season:`;
		message += `\n`;
		
		response.MRData.RaceTable.Races.map( (race, index) => {
			if (!!race.Sprint) {
				message += `\n#${race.round} (SR)`;
			} else {
				message += `\n#${race.round}`;
			}
			
			message += `\n🗺 <b>${race.raceName}</b>`;
			message += `\n🗓 ${race.date}`;
		});
		
		const option = {
			parse_mode: "HTML",
		};
		
		return ctx.reply(message, option);
		
	} catch(error) {
		console.error('racecalendar :(', error)
	}
};

module.exports = racecalendar;
