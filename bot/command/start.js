const start = () => (ctx) => {
	/**
	 * @todo improve bot presentation: describe bot features
	 * @example a bot about the current Formula 1 Championship.
	 * This bot can:
	 * - alert you about incoming races;
	 * - keep you updated about results and standings;
	 * - list race schedules;
	 */
	let message = ``;
	message += `Welcome to <b>F1 Alerts</b> 🏎⏰`;
	message += `\n`;
	message += `\n`;
	message += `Start typing "<b>/</b>" to see a list of available commands.`;
	message += `\n`;
	message += `Type /about for more info.`;
	
	const option = {
		parse_mode: "HTML",
	};
	
	return ctx.reply(message, option);
};

module.exports = start;
