const botUtil = require("../util");

const driverstandings = () => async (ctx) => {
	try {
		
		const response = await botUtil.processQuery('/current/driverStandings.json');
		
		let message = ``;
		message += `🏆 <b>Driver standings</b> (round #${response.MRData.StandingsTable.StandingsLists[0].round} of ${response.MRData.StandingsTable.season} season):`;
		message += `\n`;
		
		response.MRData.StandingsTable.StandingsLists[0].DriverStandings.map( (standing, index) => {
			message += `\n#${standing.position} <b>${standing.Driver.familyName}</b> <code>${standing.points}pts</code>`;
		});
		
		const option = {
			parse_mode: "HTML",
		};
		
		return ctx.reply(message, option);
		
	} catch(error) {
		console.error('driverstandings :(', error)
	}
};

module.exports = driverstandings;
