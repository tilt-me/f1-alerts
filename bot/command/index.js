const start = require('./start');
const nextround = require('./nextround');
const lastround = require('./lastround');
const driverstandings = require('./driverstandings');
const constructorstandings = require('./constructorstandings');
const racecalendar = require('./racecalendar');
const about = require('./about');

module.exports = {
	start,
	nextround,
	lastround,
	driverstandings,
	constructorstandings,
	racecalendar,
	about,
}
