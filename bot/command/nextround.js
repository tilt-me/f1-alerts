const botUtil = require("../util");

const nextround = () => async (ctx) => {
	try {
		
		const response = await botUtil.processQuery('/current/next.json');
		
		let message = ``;
		message += `<b>Next round</b> (#${response.MRData.RaceTable.round} of ${response.MRData.RaceTable.season} season):`;
		message += `\n`;
		
		if ( response.MRData.RaceTable.Races[0] ) {
			message += `🗺 ${response.MRData.RaceTable.Races[0].raceName}`;
			message += `\n`;
			message += `🗓 ${response.MRData.RaceTable.Races[0].date}`;
			message += `\n`;
			message += `🕰 ${response.MRData.RaceTable.Races[0].time}`;
			message += `<code>`;
			message += `\n--- ---------- ---------\n`;
			
			if ( !response.MRData.RaceTable.Races[0].Sprint ) {
				message += `FP1 ${response.MRData.RaceTable.Races[0].FirstPractice.date} ${response.MRData.RaceTable.Races[0].FirstPractice.time}`;
				message += `\n`;
				message += `FP2 ${response.MRData.RaceTable.Races[0].SecondPractice.date} ${response.MRData.RaceTable.Races[0].SecondPractice.time}`;
				message += `\n`;
				message += `FP3 ${response.MRData.RaceTable.Races[0].ThirdPractice.date} ${response.MRData.RaceTable.Races[0].ThirdPractice.time}`;
				message += `\n`;
				message += `  Q ${response.MRData.RaceTable.Races[0].Qualifying.date} ${response.MRData.RaceTable.Races[0].Qualifying.time}`;
				message += `\n`;
				message += `  R ${response.MRData.RaceTable.Races[0].date} ${response.MRData.RaceTable.Races[0].time}`;
			} else {
				message += `FP1 ${response.MRData.RaceTable.Races[0].FirstPractice.date} ${response.MRData.RaceTable.Races[0].FirstPractice.time}`;
				message += `\n`;
				message += `  Q ${response.MRData.RaceTable.Races[0].Qualifying.date} ${response.MRData.RaceTable.Races[0].Qualifying.time}`;
				message += `\n`;
				message += ` SS ${response.MRData.RaceTable.Races[0].SecondPractice.date} ${response.MRData.RaceTable.Races[0].SecondPractice.time}`;
				message += `\n`;
				message += ` SR ${response.MRData.RaceTable.Races[0].Sprint.date} ${response.MRData.RaceTable.Races[0].Sprint.time}`;
				message += `\n`;
				message += `  R ${response.MRData.RaceTable.Races[0].date} ${response.MRData.RaceTable.Races[0].time}`;
			}
			
			message += `</code>`;
			
		} else {
			message += `<i>data not yet available</i>`;
		}
		
		const option = {
			parse_mode: "HTML",
		};
		
		return ctx.reply(message, option);
		
	} catch(error) {
		console.error('nextround :(', error)
	}
};

module.exports = nextround;
