const botUtil = require("../util");

const lastround = () => async (ctx) => {
	try {
		
		const response = await botUtil.processQuery('/current/last/results.json');
		
		let message = ``;
		message += `<b>Last round</b> results (#${response.MRData.RaceTable.round} of ${response.MRData.RaceTable.season} season):`;
		message += `\n`;
		message += `🗺 ${response.MRData.RaceTable.Races[0].raceName}`;
		message += `\n`;
		message += `🗓 ${response.MRData.RaceTable.Races[0].date}`;
		message += `\n`;
		
		response.MRData.RaceTable.Races[0].Results.map( (result, index) => {
			message += `\n#${result.position} <b>${result.Driver.familyName}</b> <i>${result.Constructor.name}</i> <code>+${result.points}pts</code>`;
		});
		
		const option = {
			parse_mode: "HTML",
		};
		
		return ctx.reply(message, option);
		
	} catch(error) {
		console.error('lastround :(', error)
	}
};

module.exports = lastround;
