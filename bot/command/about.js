const { name, author, description, homepage, } = require("../package.json");

const about = () => (ctx) => {
	let message = ``;
	//message += `@${name} is developed by ${author}.`;
	message += `ℹ ${description}`;
	message += `\n`;
	message += `🌐 This app is offered as a <a href="${homepage}">website</a> as well.`;
	message += `\n`;
	message += `🙏 The whole project wouldn't exist without the <a href="https://ergast.com/mrd/">Ergast Developer API</a>.`;
	message += `\n`;
	message += `🖼 Bot's profile picture is a <a href="https://twemoji.twitter.com/">Twemoji</a>.`;
	message += `\n`;
	message += `🧑‍💻 Source code on <a href="https://gitlab.com/tilt-me/f1-alerts">GitLab</a>.`;
	
	const option = {
		disable_web_page_preview: true,
		parse_mode: "HTML",
	};
	
	return ctx.reply(message, option);
};

module.exports = about;
