const { Bot } = require("grammy");

const bot = new Bot(String(process.env.BOT_TOKEN));

module.exports = bot;
