# F1 Alerts API #

## Developing ##

Start local server (and read the logs):

```
$ npm run start
```

Server will respond under port 3000, for example you can open your browser at http://[insert your local ip]:3000/f1/current/last.json


## Publishing ##

Publishing is handled via Deta: every push to master will trigger a deployment.  
- Every year you need to create a new access token via the Deta dashboard: https://docs.deta.sh/docs/cli/auth#deta-access-tokens 
- then copy the newly generated token in gitlab:  
  Settings > CI/CD > Variables > Expand > `DETA_TOKEN`


## References ##

- A simple caching strategy for Node REST APIs, Part 1:
https://dev.to/vigzmv/a-simple-caching-strategy-for-node-rest-apis-part-1-72a
- https://medium.com/@seulkiro/deploy-node-js-app-with-gitlab-ci-cd-214d12bfeeb5
- https://medium.com/swlh/how-do-i-deploy-my-code-to-heroku-using-gitlab-ci-cd-6a232b6be2e4
- https://dustinpfister.github.io/2018/01/28/heroku-cors/
- https://itnext.io/deploying-a-vue-adonisjs-monorepo-to-heroku-via-gitlab-ci-cd-a4d87b31c20?gi=2f5cccbb07b0
- https://github.com/BogDAAAMN/deta-deploy-action/blob/main/action.yml
