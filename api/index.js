'use strict';

/**
 * @description set a monorepo to serve /web under /, /api under /api and /bot as telegram bot
 * @see http://expressjs.com/en/starter/hello-world.html
 * @see https://vercel.com/blog/monorepos
 * @see https://www.carlosroso.com/how-to-deploy-a-monorepo-in-vercel/
 * @see https://medium.com/the-node-js-collection/simple-server-side-cache-for-express-js-with-node-js-45ff296ca0f0
 */

const express = require('express');
const app = express();
const port = 3000;

const packageInfo = require('./package.json');

/**
 * @description handle cors (/web is under gitlab pages, /api is under deta)
 * @see https://www.npmjs.com/package/cors#configuring-cors-w-dynamic-origin
 * @see https://dustinpfister.github.io/2018/01/28/heroku-cors/
 */
const cors = require('cors');
const allowList = ['https://tilt-me.gitlab.io', 'http://tilt-me.gitlab.io'];
const corsOptions = {
  origin: function (origin, callback) {
    if (allowList.indexOf(origin) !== -1 || !origin) {// If you do not want to block REST tools or server-to-server requests, add a !origin check in the origin function
      callback(null, true);
    } else {
      callback(new Error('Not allowed by CORS'));
    }
  },
	optionsSuccessStatus: 200, // some legacy browsers (IE11, various SmartTVs) choke on 204
}

const cache = require('./cache');// 👈 import our cache middleware

/**
 * @description make call to external api
 * @requires bent https://github.com/mikeal/bent
 */
const bent = require('bent');

const getJSON = bent('GET', 'json', 'http://ergast.com/api', 200);

async function processQuery (req, res, next) {
	try {
		let response = await getJSON( req.originalUrl );// it works, ergast give us the desired json
		res.locals.data = response;// @see http://expressjs.com/en/api.html#res.locals
		res.locals.duration = 60 * 60;// 60m * 60s = 1h
		return next();
	} catch (error) {// Handle rejection here
		console.error('index :(', error);
		res.locals.data = {"api-error": "Ergast seems down :( please try again later"};
		res.locals.duration = 5 * 60;// 5m
		return next();
	}
}

app.get(
	'/f1/*',
	cors(corsOptions),
	cache.get,// 👈 if you have cache, skip next steps and send response to client
	processQuery,// no cache, so fetch 3rd party api
	//productsController.index,// unneeded step?
	cache.set,// 👈 save cache for next time and send response to client
)

app.get('/', function (req, res) {
	//console.log(req.originalUrl);
	res.json({
		name: packageInfo.name,
	});
	
});

const server = app.listen(parseInt(process.env.PORT) || port, () => {
	const host = server.address().address;
	const port = server.address().port;
	console.log('Web server started at http://%s:%s', host, port);
});
