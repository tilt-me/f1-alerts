/**
 * @see https://dev.to/vigzmv/a-simple-caching-strategy-for-node-rest-apis-part-1-72a
 */
const NodeCache = require( "node-cache" );

const myCache = new NodeCache({
	// stdTTL: time to live in seconds for every generated cache element.
	stdTTL: 5 * 60,// 5 * 60s = 5m
});

function getUrlFromRequest(req) {
  //const url = req.protocol + '://' + req.headers.host + req.originalUrl;
  const url = req.protocol + '://' + 'ergast.com/api' + req.originalUrl;
  return url;
}

function get(req, res, next) {
	const url = getUrlFromRequest(req);
	//console.log('get url: '+ url);
	const content = myCache.get(url);
	if (content) {// if we have cache, skip next steps
		console.log('we have cache! :)');
		return res.status(200).send(content);
	}
	return next();
}

function set(req, res, next) {
  const url = getUrlFromRequest(req);
	//console.log('set url: '+ url);
	//console.log( res.locals.data );
	//console.log( res.locals.duration );
  myCache.set(url, res.locals.data, res.locals.duration);// overwrite default cache duration
	console.log('no cache, fetch from api ;)');
	return res.status(200).send(res.locals.data);
  //return next();
}

/**
 * export single methods
 * @see https://www.tutorialsteacher.com/nodejs/nodejs-module-exports
 */
module.exports = { get, set };

/**
 * export whole function
 * @see https://www.tutorialsteacher.com/nodejs/nodejs-module-exports
 */
/* 
module.exports = function (duration) {
	
	return (req, res, next) => {
		console.log(req.originalUrl);
		console.log(req.url);
		let key = '__express__' + req.originalUrl || req.url
		const cachedBody = myCache.get(key);
		
		if (cachedBody) {
			
			console.log('fetch from cache');
      res.send(cachedBody)
      return;
			
    } else {
			console.log('fetch from api');
      res.sendResponse = res.send;
      res.send = (body) => {
        myCache.set(key, body, duration);
        res.sendResponse(body)
      }
      next();
    }
  }
};
 */

/**
 * Handle cache logics
 * @param {*} ctx
 * @param {string} currentPath
 * @param {string} currentEvent `f1Response` || `f1Alert`
 * @todo add a @param (event) to handle calls different from f1response
 * you need this function to handle @see enableAlerts
 */
/* 
const fetchCache = function(ctx, currentPath, currentEvent){

	let currentKey = currentPath.split('.json')[0].replace(/\//g, "_");
	//console.log( currentKey );

	myCache.get( currentKey, function( err, cachedBody ){

		if ( !err ){

			if (cachedBody == undefined){// no cached value, so fetch it from API

				console.log('fetch from api');
				f1Request(currentPath, (err, response, body) => {

					if ( !err ) {

						console.log(`API call success :)`);
						myCache.set( currentKey, body );
						myEmitter.emit(currentEvent, ctx, currentPath, body);

					} else {
						console.log( err );
						console.log(`API call failed :(`);
					}

				});

			} else {// we have a cached body

				console.log('fetch from cache');
				myEmitter.emit(currentEvent, ctx, currentPath, cachedBody);

			}

		} else {
			console.log(`error in myCache.get`);
			console.log(err);
		}

	});

};
 */
