# F1 Alerts Web #

This is the project you get when you run `npx gridsome create web`.


## Scaffolding ##

No need to globally install gridsome-cli, just use `npx` to install default starter:
```
$ npx gridsome create new-project
```
Then `cd new-project` to open the folder.


## Developing ##

Start a local dev server at `http://localhost:8080`:
```
$ npm run develop
```

Happy coding 🎉🙌


## Publishing ##

Publishing is handled via GitLab Pages: every push to master will trigger a deployment.  


## References ##

- https://medium.com/swlh/on-monorepos-and-the-deployment-with-gitlab-ci-cd-bc080cfc6dce
