// Server API makes it possible to hook into various parts of Gridsome
// on server-side and add custom data to the GraphQL data layer.
// Learn more: https://gridsome.org/docs/server-api/

// Changes here require a server restart.
// To restart press CTRL + C in terminal and run `gridsome develop`

module.exports = function (api) {
  api.loadSource(({ addCollection }) => {
    // Use the Data Store API here: https://gridsome.org/docs/data-store-api/
  })

  api.createPages(({ createPage }) => {
    // Use the Pages API here: https://gridsome.org/docs/pages-api/
  })
  
  /**
   * @description during local development, we start /web under :8080 port, while /api under :3000 port
   * so we need to redirect calls from http://localhost:8080/api/* to http://localhost:3000/
   * @see https://gridsome.org/docs/server-api/#apiconfigureserverfn
   * Gridsome runs an Express server during development.
   * Use this hook (configureServer) to add custom endpoints or configure the server.
   */
  api.configureServer(app => {
    
    const bent = require('bent');
    const getJSON = bent('GET', 'json', 'http://localhost:3000', 200);
    
    app.get('/api/*', async (req, res) => {
      //console.log( req.originalUrl );
      //res.send('Hello, world!');
      const cleanedUrl = req.originalUrl.replace('/api', '');
      
      try {
        //let response = await getJSON('/f1/current.json');
        let response = await getJSON( cleanedUrl );
        //console.log( response );
        res.locals.data = response;
        res.status(200).send( res.locals.data );
      } catch (error) {// Handle rejection here
        console.error('get /api :(', error)
        res.status(500).send('get /api error :(');
      }
      
    });
    
  });
}
