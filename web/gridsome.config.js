// This is where project configuration and plugin options are located.
// Learn more: https://gridsome.org/docs/config

// Changes here require a server restart.
// To restart press CTRL + C in terminal and run `gridsome develop`

module.exports = {
  siteName: 'F1 Alerts',
  pathPrefix: '/f1-alerts/',
  icon: {
    favicon: {
      src: './src/favicon.png',
      sizes: [96],
    },
    touchicon: {
      src: './src/favicon.png',
      sizes: [180],
    },
  },
  plugins: [],
}
