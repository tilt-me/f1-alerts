# F1 Alerts #

Monorepo to handle F1 alerts:
- [/api](/api) contains the source code to cache Ergast API; both client interfaces (web and bot) query these API;
- [/web](/web) contains the source code of the web client;
- [/bot](/bot) contains the source code of the Telegram Bot.


## Acknowledgments ##

Without [Ergast Developer API](http://ergast.com/mrd/) this project would not exist, thanks a lot! :pray:
